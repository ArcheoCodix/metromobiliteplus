# MetromobilitePlus
## Features possibles
- Menu accessible partout :
  - Horaires
  - Autour de moi
  - Itineraire 
  - Mon compte
  - Paramètres
  - A propos
  - Alerter
- Page d'accueil :
  1. Favoris
  2. Informations trafic
  3. Atmo

## Features prévues
- Menu :
  - Horaires
  - Autour de moi
  - Itineraire
  
- Page d'accueil :
  - Favoris
  - Informations trafic
