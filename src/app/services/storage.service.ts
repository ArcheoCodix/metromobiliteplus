import { Injectable } from '@angular/core';
import { Plugins } from '@capacitor/core';
import {Line} from '../models/line';
import {FavouriteStation} from '../models/favourite-station';
const { Storage } = Plugins;

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  // tslint:disable-next-line:variable-name
  private _lines: Line[] = [];
  // tslint:disable-next-line:variable-name
  private _favorites: FavouriteStation[] = [];

  private _typeIndex: string[] = [];

  constructor() {
    this.loadFavorites();
  }

  public async loadFavorites() {
    Storage.get({key: 'favorites'}).then((result) => {
      const res: FavouriteStation[] = [];
      if (result.value !== null) {
        for (const obj of JSON.parse(result.value)) {
          res.push(new FavouriteStation(obj, obj.otherWay, obj.line));
        }
      }
      this._favorites = res;
    });
  }

  public async setFavorites(data: FavouriteStation[]) {
    await Storage.set({
      key: 'favorites',
      value: JSON.stringify(data)
    });
  }

  get lines(): Line[] {
    return this._lines;
  }

  set lines(value: Line[]) {
    this._lines = value;
    this._typeIndex = [];
    this.loadTypeIndex();
  }

  get favorites(): FavouriteStation[] {
    return this._favorites;
  }

  get sortedLines(): Line[][] {
    let i = -1;
    const lineList: Line[][] = [];

    this.lines.forEach(line => {
      if (!(lineList[i] !== undefined && line.type === lineList[i][0].type)) {
        i++;
        lineList[i] = [];
        this._typeIndex.push(line.typeToString());
      }
      lineList[i].push(line);
    });

    return lineList;
  }

  private loadTypeIndex() {
    let i = -1;

    this.lines.forEach(line => {
      if (this._typeIndex.indexOf(line.typeToString()) < 0) {
        i++;
        this._typeIndex.push(line.typeToString());
      }
    });
  }

  public getTypeIndex(type: string) {
    return this._typeIndex.indexOf(type);
  }
}
