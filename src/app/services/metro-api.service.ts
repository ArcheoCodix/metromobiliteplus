import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Line} from '../models/line';
import {StorageService} from './storage.service';
import {LineHours} from '../models/line-hours';
import {PointType} from '../models/point-type';
import {SearchedStation} from '../models/searched-station';
import {StationHours} from '../models/station-hours';
import {GeolocationPosition} from '@capacitor/core';
import { Plugins } from '@capacitor/core';
import { DataType } from '../models/data-type';
import { indiceAtmoFull, Parking, evt } from '../models/dynamic-data-type';
const { Geolocation } = Plugins;
import {SearchedParking} from '../models/searched-parking';
import {SearchedEntity} from '../models/searched-entity';
import {SearchedAgency} from '../models/searched-agency';

@Injectable({
    providedIn: 'root'
})
export class MetroAPIService {
    public static filterLines(lines: any[], types = ['PROXIMO', 'FLEXO', 'TRAM', 'C38']) {
        const res = [];
        for (const line of lines) {
            if (line.hasOwnProperty('type') && types.includes(line.type)) {
                res.push(Object.assign(new Line(), line) as Line);
            }
        }
        return res;
    }

    constructor(private requester: HttpClient, private storage: StorageService) {}

    public nearbyStations(location: GeolocationPosition, radius = 400, resultCallback: (data) => void) {
        this.requester.get(
            // tslint:disable-next-line:max-line-length
        `http://data.metromobilite.fr/api/linesNear/json?x=${location.coords.longitude}&y=${location.coords.latitude}&dist=${Math.floor(radius * 1.4)}&details=true`
        ).toPromise().then(result => {
            resultCallback(result);
        });
    }

    public getLines() {
    return this.requester.get('http://data.metromobilite.fr/api/routers/default/index/routes').toPromise()
        .then((lines: any[]) => {
          this.storage.lines = MetroAPIService.filterLines(lines);
          return this.storage.lines;
        });
    }

    public getLinesHours(idLine: string, timeInms: number) {
      return this.requester.get(`http://data.metromobilite.fr/api/ficheHoraires/json?route=${idLine}&time=${timeInms}`).toPromise()
          .then(res => {
            return new LineHours(res);
          })
          .catch(error => {
            throw error;
          });
    }

    public getStationHours(idStation: string) {
    return this.requester.get(`http://data.metromobilite.fr/api/routers/default/index/clusters/${idStation}/stoptimes`).toPromise()
        .then((res: any[]) => {
            const results = [];

            res.forEach(oneStationHours => {
                results.push(new StationHours(oneStationHours));
            });

            return results;
        })
        .catch(error => {
            throw error;
        });
    }

    public getPostHours(idStation: string): Promise<StationHours[]> {
        return this.requester.get(`http://data.metromobilite.fr/api/routers/default/index/stops/${idStation}/stoptimes/`).toPromise()
            .then((res: any[]) => {
                const results = [];

                res.forEach(oneStationHours => {
                    results.push(new StationHours(oneStationHours));
                });

                return results;
            })
            .catch(error => {
                throw error;
            });
    }

    public searchPoint(types: string, query = '') {
        return this.requester.get(`http://data.metromobilite.fr/api/findType/json?types=${types}&query=${query}`).toPromise()
            .then((res: {features: any[]}) => {
                const results = [];

                res.features.forEach(feat => {
                    switch (feat.properties.type) {
                        case 'arret':
                            results.push(new SearchedStation(feat));
                            break;

                        case 'PKG':
                            results.push(new SearchedParking(feat));
                            break;

                        case 'PAR':
                            results.push(new SearchedParking(feat));
                            break;

                        case 'recharge':
                            results.push(new SearchedEntity(feat));
                            break;

                        case 'agenceM':
                            results.push(new SearchedAgency(feat));
                            break;

                        default:
                            console.warn(`searched type ${feat.properties.type} unknown`);
                            break;
                    }
                });

                return results;
            })
            .catch(error => {
              throw error;
            });
    }

    public dynamicData(type: DataType): any{
        return this.requester.get(`http://data.metromobilite.fr/api/dyn/${DataType[type]}/json`).toPromise()
            .then((res: any)=>{
                const result = [];

                switch(type){
                    case DataType.indiceAtmoFull:
                        result.push(new indiceAtmoFull(res));
                        break;
                        
                    case DataType.PAR:
                        Object.keys(res).map(praking =>{
                            result.push(res[praking]);
                        })
                        break;

                    case DataType.evt:
                        Object.keys(res).map(traf =>{
                            result.push(res[traf]);
                        })
                        break;
                }
                return result;
            })
            .catch(error => {
                throw error;
              });
    }
}
