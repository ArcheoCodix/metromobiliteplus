import { Component, OnInit } from '@angular/core';
import {NavParams, PopoverController} from '@ionic/angular';
import {Line} from 'src/app/models/line';
import {NearbyStation} from '../../models/nearby-station';
import {Router} from "@angular/router";

@Component({
  selector: 'app-marker-popover',
  templateUrl: './marker-popover.component.html',
  styleUrls: ['./marker-popover.component.scss'],
})
export class MarkerPopoverComponent implements OnInit {

  private station: NearbyStation;

  constructor(private navParams: NavParams, private router: Router, private popoverCtrl: PopoverController) { }

  ngOnInit() {
    this.station = this.navParams.get('station');
  }

  private getButtonStyle(line: Line) {
    return {
      'background-color': `#${line.color}`,
      color: `#${line.textColor}`,
      'border-radius': '5px',
    };
  }

  private popOverStationClick(line: Line) {
      this.popoverCtrl.dismiss();
      this.router.navigate(['/hours-detail', { idLine: line.id, type: line.typeToString() }]);
  }
}
