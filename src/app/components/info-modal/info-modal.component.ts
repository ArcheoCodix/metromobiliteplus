import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { StorageService } from '../../services/storage.service';
import { MetroAPIService } from '../../services/metro-api.service';
import { DataType } from '../../models/data-type';

@Component({
  selector: 'app-info-modal',
  templateUrl: './info-modal.component.html',
  styleUrls: ['./info-modal.component.scss'],
})
export class InfoModalComponent implements OnInit {


  private ITraftype: DataType;
  private ITrafic: any;
  private ITraficV: any;
  private ITraficT: any;
  private valeurSeg = "voiture";
  constructor(private modalCtrl: ModalController, private API: MetroAPIService, private storage: StorageService, private NavParams: NavParams) {}

  ngOnInit() {
    this.ItraInit();
  }

  async ItraInit(){
    this.ITraftype = this.NavParams.get('infTraf');
    this.ITrafic = await this.API.dynamicData(this.ITraftype);
    this.ITraficV = this.ITrafic.filter(val => val.visibleVoiture);
    this.ITraficT = this.ITrafic.filter(val => val.visibleTC);
  }

  public closeModal(){
    this.modalCtrl.dismiss();
  }

  public segmentChanged(event: any){
    this.valeurSeg = event.detail.value;
  }
}
