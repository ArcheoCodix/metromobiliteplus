import {Component, OnChanges, OnInit} from '@angular/core';
import {Map, tileLayer, circle, layerGroup, circleMarker, marker, divIcon, CircleMarker, point, DivIcon, Marker} from 'leaflet' ;
import {MetroAPIService} from '../../services/metro-api.service';
import {NearbyStation} from '../../models/nearby-station';
import {CallbackID, GeolocationPosition, Plugins} from '@capacitor/core';
import {StorageService} from '../../services/storage.service';
import {Line} from '../../models/line';
import {PopoverController} from '@ionic/angular';
import {MarkerPopoverComponent} from '../../components/marker-popover/marker-popover.component';
const { Geolocation } = Plugins;

@Component({
  selector: 'app-nearby',
  templateUrl: './nearby.page.html',
  styleUrls: ['./nearby.page.scss'],
})
export class NearbyPage implements OnInit {

  private map: Map;
  private nearby: NearbyStation[] = [];
  private gps: GeolocationPosition;
  private gpsMarker: CircleMarker;
  private gpsWatcher: CallbackID;
  private mapLayers;
  private radius = 400;
  private isLoading = false;

  constructor(private api: MetroAPIService, private storage: StorageService, private popoverController: PopoverController) { }

  ngOnInit() {
    this.loadMap();
  }

  private async loadMap() {
    this.gps = await Geolocation.getCurrentPosition({enableHighAccuracy: true, requireAltitude: false});
    this.map = new Map('nearby-map').setView([this.gps.coords.latitude, this.gps.coords.longitude], 17);
    this.mapLayers = layerGroup().addTo(this.map);

    tileLayer('https://cartodb-basemaps-{s}.global.ssl.fastly.net/rastertiles/voyager/{z}/{x}/{y}.png', {
      maxZoom: 18
      }).addTo(this.map);

    this.map.on('load', () => {
      this.map.invalidateSize();
    });

    this.searchNearby();
  }

  private searchNearby(event?) {
    // Using arrow function to stay in this class's scope
    this.isLoading = true;
    this.api.nearbyStations(this.gps, this.radius, (data) => this.updateMarkers(data));
  }

  private updateMarkers(results: any) {
    try {
      this.nearby = [];
      for (const result of results) {
        const newobj = new NearbyStation(result, this.storage.lines);
        if (newobj.lines.length > 0) {
          this.nearby.push(newobj);
        }
      }
    } catch (e) {
      console.error(e);
    }

    this.mapLayers.clearLayers();
    circle([this.gps.coords.latitude, this.gps.coords.longitude], {
      color: 'blue',
      fillColor: 'lightblue',
      fillOpacity: 0.25,
      radius: this.radius
    }).addTo(this.mapLayers);

    this.gpsMarker = circleMarker([this.gps.coords.latitude, this.gps.coords.longitude], {
      color: 'red',
      fillColor: '#f22',
      fillOpacity: 0.5
    }).addTo(this.mapLayers);

    Geolocation.clearWatch({id: this.gpsWatcher});
    this.gpsWatcher = Geolocation.watchPosition(
        {
          enableHighAccuracy: true,
          maximumAge: 20000,
          requireAltitude: false,
          timeout: 10000
        },
        (result) => {
      this.gpsMarker.setLatLng([result.coords.latitude, result.coords.longitude]);
    });

    for (const stationPoint of this.nearby) {
      marker([stationPoint.location.lat, stationPoint.location.lng], {
        icon: divIcon(
            {
              html: stationPoint.lines[0].mode.toString() === 'TRAM' ?
                  `<ion-icon name="train" style="width: 1.5rem; height: 1.5rem"></ion-icon>` :
                  `<ion-icon name="bus" style="width: 1.5rem; height: 1.5rem;"></ion-icon>`,
              className: 'map-station-icon',
            }
        )
      }).on('click', (event: any) => { this.openPopover(event.originalEvent, stationPoint); }).addTo(this.mapLayers);
    }
    this.isLoading = false;
  }

  private async openPopover(event: any, stationPoint: any) {
    const popover = await this.popoverController.create({
      component: MarkerPopoverComponent,
      componentProps: {station: stationPoint},
      event,
      translucent: true,
      mode: 'ios'
    });
    return await popover.present();
  }

  ionViewWillLeave() {
    Geolocation.clearWatch({id: this.gpsWatcher});
  }
}
