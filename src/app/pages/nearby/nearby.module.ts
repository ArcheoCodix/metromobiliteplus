import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { NearbyPage } from './nearby.page';
import {MarkerPopoverComponent} from '../../components/marker-popover/marker-popover.component';

const routes: Routes = [
  {
    path: '',
    component: NearbyPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [NearbyPage, MarkerPopoverComponent],
  entryComponents: [MarkerPopoverComponent]
})
export class NearbyPageModule {}
