import { Component, OnInit } from '@angular/core';
import {MetroAPIService} from '../../services/metro-api.service';
import {SearchedStation} from '../../models/searched-station';
import {SearchedParking} from '../../models/searched-parking';
import {SearchedAgency} from '../../models/searched-agency';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {

  private pointSearchType = 'arret';
  private searchValue: string;
  private searchResults: any[];
  private isSearching = false;
  private searchEverything = false;

  constructor(private api: MetroAPIService) { }

  ngOnInit() {
  }

  private async updateSearch() {
    if (this.searchValue.length > 0 || this.searchEverything) {
      this.isSearching = true;
      this.searchResults = await this.api.searchPoint(this.pointSearchType, this.searchValue);
      this.isSearching = false;
    }
  }

  private getType(item: any) {
    switch (item.constructor) {
      case SearchedStation:
        return 'arret';

      case SearchedParking:
        return 'parking';

      case SearchedAgency:
        return 'agency';

      default:
        return 'generic';
    }
  }

  private handleTypes() {
    switch (this.pointSearchType) {
      case 'arret':
        this.searchValue = '';
        this.searchEverything = false;
        break;

      case 'PAR,PKG':
        this.searchEverything = true;
        this.searchValue = '';
        break;

      case 'recharge':
        this.searchEverything = true;
        this.searchValue = '';

      case 'agenceM':
        this.searchEverything = true;
        this.searchValue = '';
    }

    this.updateSearch();
  }
}
