import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InfoModalComponent } from '../../components/info-modal/info-modal.component';
import { DataType } from '../../models/data-type';
import { MetroAPIService } from '../../services/metro-api.service';
import { StorageService } from '../../services/storage.service';
import { indiceAtmoFull } from '../../models/dynamic-data-type';
import {FavouriteStation} from '../../models/favourite-station';
import {StationHours} from '../../models/station-hours';
import * as moment from 'moment';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage {

  private IAtmo: indiceAtmoFull[];
  private expanded = false;
  favourites: FavouriteStation[];
  hoursFavourites: StationHours[][] = [];
  readonly today = moment().hour(0).minute(0).second(0).unix();

  constructor(private modal: ModalController, private API: MetroAPIService, private storage: StorageService) {}

  ngOnInit() {
    this.atmoInit();
    this.loadFavouritesHours();
  }

  async traficInfoModal(){
    const myModal = await this.modal.create({
      component: InfoModalComponent,
      componentProps: {infTraf: DataType.evt}
    });

    return await myModal.present();
  }

  async loadFavouritesHours() {
    this.favourites = this.storage.favorites;

    this.favourites.forEach((fav, index) => {
      this.hoursFavourites.push([]);
      this.API.getPostHours(fav.id).then( res => {
        this.hoursFavourites[index].push(res[0]);
        this.hoursFavourites[index].push(res[1]);
        console.log(this.hoursFavourites);
      });
    });
  }

  async atmoInit(){
    this.IAtmo = await this.API.dynamicData(DataType.indiceAtmoFull);
  }

  hasFavourite() {
    return this.favourites.constructor === Array && this.favourites.length > 0;
  }

  hasManyFavourite() {
    return this.favourites.constructor === Array && this.favourites.length > 3;
  }

  midnightTimestampToString(timestamp: number) {
    const passTime = moment.unix(this.today).add(timestamp, 'seconds');
    const diff = passTime.local().diff(moment(), 'seconds');
    let res: string;

    if ( diff / 3600 > 1) {
      res = passTime.format('HH:mm');
    } else {
      if (diff > 60) {
        res = Math.floor(diff / 60) + ' min';
      } else {
        res = '< 1 min';
      }
    }

    return res;
  }
}
