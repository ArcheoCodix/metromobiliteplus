import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {Line} from '../../models/line';
import {LineHours} from '../../models/line-hours';
import {MetroAPIService} from '../../services/metro-api.service';
import * as moment from 'moment';
import {StorageService} from '../../services/storage.service';
import {Stop} from '../../models/stop';
import {FavouriteStation} from '../../models/favourite-station';

@Component({
  selector: 'app-hours-detail',
  templateUrl: './hours-detail.page.html',
  styleUrls: ['./hours-detail.page.scss'],
})
export class HoursDetailPage implements OnInit {

  nonshowedLinesHours: Line[][];
  showedLinesHours: { line: Line, hours: LineHours}[] = [];
  currentSegment: string;
  loaded = false;
  spinners = {};
  readonly today = moment().utc().hour(0).minute(0).second(0).unix();
  favorites: any = {};
  dateTime: any;

  constructor(
      private route: ActivatedRoute,
      private metroAPI: MetroAPIService,
      private storageSrv: StorageService
  ) { }

  ngOnInit() {
    this.nonshowedLinesHours = this.storageSrv.sortedLines;

    this.route.params.subscribe(params => {
      let idType: number;

      if (params.idType === undefined) {
        idType = this.storageSrv.getTypeIndex(params.type);
      } else {
        idType = params.idType;
      }

      if (params.zoneCode !== undefined) {

      }

      this.addLineHours(params.idLine, idType);
    });
  }

  async addLineHours(idLine: string, idType: number, selected: boolean = false) {
    if (selected) {
      if (this.showedLinesHours.length > 1) {
        const index = this.showedLinesHours.findIndex(line => line.line.id === idLine);
        if (index >= 0) { this.showedLinesHours.splice(index, 1); }

        if (idLine === this.currentSegment) { this.currentSegment = this.showedLinesHours[0].line.id; }
        this.spinners[idLine] = undefined;
        this.favorites[idLine] = undefined;
      }
    } else {
      if (this.showedLinesHours.length < 5) {
        this.spinners[idLine] = false;
        const paramLine = this.nonshowedLinesHours[idType].find(searchedline => searchedline.id === idLine);
        const lineHours = {
          line: paramLine,
          hours: undefined
        };

        const now = moment();

        this.dateTime = now.toISOString();

        try {
          lineHours.hours = await this.metroAPI.getLinesHours(lineHours.line.id, now.valueOf());
        } catch (err) {
          console.error(err);
        }

        this.showedLinesHours.push(lineHours);
        this.currentSegment = lineHours.line.id;
        this.favorites[idLine] = {};

        lineHours.hours.wayOne.arrets.forEach((stop: Stop) => {
          this.favorites[idLine][stop.zoneCode] = this.storageSrv.favorites.findIndex(fav => fav.zone === stop.zoneCode) >= 0;
        });

        lineHours.hours.wayTwo.arrets.forEach((stop: Stop) => {
          this.favorites[idLine][stop.zoneCode] = this.storageSrv.favorites.findIndex(fav => fav.zone === stop.zoneCode) >= 0;
        });

        this.spinners[idLine] = true;
        this.loaded = true;
      }
    }
  }

  midnightTimestampToString(timestamp: number) {
    const passTime = moment.unix(this.today).add(timestamp, 'seconds');
    const diff = passTime.local().diff(moment(), 'seconds');
    let res: string;

    if ( diff / 3600 > 1) {
      res = passTime.format('HH:mm');
    } else {
      if (diff > 60) {
        res = Math.floor(diff / 60) + ' min';
      } else {
        res = '< 1 min';
      }
    }

    return res;
  }

  isPassed(timestamp: number) {
    return moment.unix(this.today).add(timestamp, 'seconds').local().diff(moment(), 'seconds') <= 0;
  }

  getPart(lines: Line[], multi: number): Line[] {
    const middle = Math.ceil(lines.length / 2);

    return lines.slice(
        multi * middle,
        multi * middle + middle
    );
  }

  changeTime(line: { line: Line, hours: LineHours}, next: boolean) {
    let timestamp: number;

    if (next) {
      timestamp = line.hours.currentWay.nextTime;
    } else {
      timestamp = line.hours.currentWay.prevTime;
    }

    this.dateTime = moment(timestamp).toISOString();
    this.loadTime(line, timestamp);
  }

  async loadTime(line: { line: Line, hours: LineHours}, time: number | string) {
    const isWayOne = line.hours.isWayOne;
    let timestamp: number;

    if (typeof(time) ===  'number') {
      timestamp = (time as number);
    } else {
      timestamp = moment((time as string)).valueOf();
    }

    try {
      line.hours = await this.metroAPI.getLinesHours(line.line.id, timestamp);
    } catch (err) {
      console.error(err);
    }

    if (!isWayOne) {
      line.hours.swapWay();
    }
  }

  async fav(line: { line: Line, hours: LineHours}, zoneCode: string) {
    const favs = line.hours.favStop(zoneCode);
    const favourite = new FavouriteStation(favs.fav, favs.otherFav, line.line);
    const idFav = this.storageSrv.favorites.findIndex(fav => fav.zone === zoneCode);

    if (idFav >= 0) {
      this.storageSrv.favorites.splice(idFav, 1);
      this.favorites[line.line.id][zoneCode] = false;
    } else {
      this.storageSrv.favorites.push(favourite);
      this.favorites[line.line.id][zoneCode] = true;
    }

    this.storageSrv.setFavorites(this.storageSrv.favorites);
  }
}
