import {Component, OnInit, SimpleChange} from '@angular/core';
import {StorageService} from '../../services/storage.service';
import {Line} from '../../models/line';
import {Events} from '@ionic/angular';
import {Router} from '@angular/router';

@Component({
  selector: 'app-hours',
  templateUrl: './hours.page.html',
  styleUrls: ['./hours.page.scss'],
})
export class HoursPage implements OnInit {

  lineList: Line[][] = [];
  loaded = false;
  spinner: any = {};

  constructor(
      private storageSrv: StorageService,
      private events: Events,
      private router: Router
  ) { }

  ngOnInit() {
    if (this.storageSrv.lines.length > 0) {
      this.lineList = this.storageSrv.sortedLines;
      this.loaded = true;
    } else {
      this.events.subscribe('lines:loaded', () => {
        this.lineList = this.storageSrv.sortedLines;
        this.loaded = true;
      });
    }
  }

  selectLine(selectedLine: Line, idType: number) {
    this.spinner[selectedLine.id] = true;
    this.router.navigate(['/hours-detail', { idLine: selectedLine.id, idType }])
        .then( res => {
          this.spinner[selectedLine.id] = false;
        });
  }
}
