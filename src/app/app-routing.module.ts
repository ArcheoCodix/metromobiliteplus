import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './pages/home/home.module#HomePageModule'
  },
  { path: 'nearby', loadChildren: './pages/nearby/nearby.module#NearbyPageModule' },
  { path: 'hours', loadChildren: './pages/hours/hours.module#HoursPageModule' },
  { path: 'hours-detail', loadChildren: './pages/hours-detail/hours-detail.module#HoursDetailPageModule' },
  { path: 'search', loadChildren: './pages/search/search.module#SearchPageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
