import { Component } from '@angular/core';

import {Events, Platform} from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import {MetroAPIService} from './services/metro-api.service';
import {StorageService} from './services/storage.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [
    {
      title: 'Accueil',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Horaires',
      url: '/hours',
      icon: 'time'
    },
    {
      title: 'Rechercher',
      url: '/search',
      icon: 'search'
    },
    {
      title: 'Autour de moi',
      url: '/nearby',
      icon: 'locate'
    },
    {
      title: 'Itinéraire',
      url: '/navigate',
      icon: 'navigate'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private api: MetroAPIService,
    private storage: StorageService,
    private events: Events
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.api.getLines()
        .then(() => {
          this.events.publish('lines:loaded');
        });
    this.storage.loadFavorites();
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
