enum LineMod {
    BUS,
    TRAM,
    RAIL
}

export enum LineType {
    TRAM = 'Trams',
    C38 = 'Transisère',
    CHRONO = 'Chrono',
    FLEXO = 'Flexo',
    PROXIMO = 'Proximo'
}

export class Line {
    id: string;
    description: string;
    shortName: string;
    longName: string;
    color: string;
    textColor: string;
    mode: LineMod;
    type: LineType;

    public typeToString() {
        return LineType[this.type];
    }

    public getStyle(selected: boolean = false) {
        return {
            'background-color': '#' + (selected ? 'A6A6A6' : this.color),
            color: '#' + (selected ? 'C6C6C6' : this.textColor)
        };
    }
}
