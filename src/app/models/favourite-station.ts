import {Location} from './location';
import {Line} from './line';

export class FavouriteStation {
    public id: string;
    public name: string;
    public zone: string;
    public line: Line;
    public location: Location;
    public way: string;
    public otherWay: FavouriteStation;

    constructor(
        reqRes: {
            id: string,
            name: string,
            zone: string,
            lat: number,
            long: number,
            way: string
        }, otherWay: {
          id: string,
          name: string,
          zone: string,
          lat: number,
          long: number,
          way: string
        } = undefined,
        line: Line = undefined) {

        if (reqRes) {
          this.id = reqRes.id;
          this.name = reqRes.name;
          this.zone = reqRes.zone;
          this.location = new Location(reqRes.lat, reqRes.long);
          this.way = reqRes.way;
        }
        if (otherWay) { this.otherWay = new FavouriteStation(otherWay, undefined, line); }
        this.line = line;
    }
}
