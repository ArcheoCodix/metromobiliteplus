import { Timestamp } from 'rxjs';

export class indiceAtmoFull{
    dateModification: string;
    dispositifEnCours: string;
    polluantMajoritaire: string;
    indiceExpositionSensible: IES[];
    commentaire: string;

    constructor(data: any){
        this.dateModification = data.date_modification;
        this.dispositifEnCours = data.dispositif_en_cours;
        this.polluantMajoritaire = data.polluant_majoritaire;
        this.indiceExpositionSensible = [];

        data.indice_exposition_sensible.forEach(element => {
            this.indiceExpositionSensible.push(new IES(element));
        });

        this.commentaire = data.commentaire;
    }

}

export class PAR{
    gam: Parking[];

    constructor(gam:Parking[]) {
        const time = Math.max.apply(Math, gam.map(function(o){
            return o.time;
        }))
        
        gam.find(function(gam){
            return gam.time === time;
        })
    }
}

export class evt{
    type: string;
    id: Number;
    dateDebut: string;
    dateFin: string;
    heureDebut: string;
    heureFin: string;
    latitude: number;
    longitude: number;
    weekEnd: string;
    listeLigneArret: string;
    texte: string;

    constructor(info: any){
        this.type = info.type;
        this.id = info.id;
        this.dateDebut = info.dateDebut;
        this.dateFin = info.dateFin;
        this.heureDebut = info.heureDebut;
        this.heureFin = info.heureFin;
        this.latitude = info.latitude;
        this.longitude = info.longitude;
        this.weekEnd = info.weekEnd;
        this.listeLigneArret = info.listeLigneArret;
        this.texte = info.texte;
    }
}

export class IES{
    date: string;
    valeur: string;
    couleur: string;
    qualificatif: string;

    constructor(expoSens: any) {
        this.date = expoSens.date;
        this.valeur = expoSens.valeur;
        this.couleur = expoSens.couleur_html;
        this.qualificatif = expoSens.qualificatif;
    }
}

export class Parking{
    time: Number;
    dispo: Number;
    nsv_id: Number;

    constructor(time: Number, dispo: Number, nsv_id: Number){
        this.time = time;
        
        if(dispo != 0){
            this.dispo = dispo;
        }
        else{
            this.dispo = -1;
        }

        if(nsv_id = 0){
            this.nsv_id = nsv_id;
        }
        else{
            this.nsv_id = 0;
        }
    }

}
