import {Stop} from './stop';

class LineWay {
    arrets: Stop[];
    prevTime: number;
    nextTime: number;

    constructor(way) {
        this.prevTime = way.prevTime;
        this.nextTime = way.nextTime;
        this.arrets = [];

        way.arrets.forEach(stop => {
            this.arrets.push(new Stop(stop));
        });
    }
}

export class LineHours {
    private wayOne: LineWay;
    private wayTwo: LineWay;
    private _isWayOne: boolean;

    constructor(hours) {
        this.wayOne = new LineWay(hours['0']);
        this.wayTwo = new LineWay(hours['1']);
        this._isWayOne = true;
    }

    get isWayOne() {
        return this._isWayOne;
    }

    get currentWay() {
        if (this._isWayOne) {
            return this.wayOne;
        } else {
            return this.wayTwo;
        }
    }

    public wayName(isWayOne = this._isWayOne) {
        if (isWayOne) {
            return this.wayOne.arrets[this.wayOne.arrets.length - 1].stopName;
        } else {
            return this.wayTwo.arrets[this.wayTwo.arrets.length - 1].stopName;
        }
    }

    public swapWay() {
        this._isWayOne = !this._isWayOne;
    }

    public favStop(zoneCode: string) {
        const stopOne = this.wayOne.arrets.find(stop => stop.zoneCode === zoneCode);
        const stopTwo = this.wayTwo.arrets.find(stop => stop.zoneCode === zoneCode);

        const fav = {id: stopOne.stopId, name: stopOne.stopName, zone: stopOne.zoneCode, lat: stopOne.stopLocation.lat, long: stopOne.stopLocation.lng, way: this.wayName(true)};
        const otherFav = {id: stopTwo.stopId, name: stopTwo.stopName, zone: stopTwo.zoneCode, lat: stopTwo.stopLocation.lat, long: stopTwo.stopLocation.lng, way: this.wayName(false)};



        return {fav, otherFav};
    }
}
