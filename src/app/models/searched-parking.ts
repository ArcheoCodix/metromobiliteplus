import {SearchedEntity} from './searched-entity';

export class SearchedParking extends SearchedEntity {
    totalSlots: number;
    type: string;
    address: string;

    constructor(searched: any) {
        super(searched);
        this.totalSlots = searched.properties.TOTAL;
        this.type = searched.properties.TYPE === 'PAR' ? 'Parking Relais' : 'Parking normal';
    }
}
