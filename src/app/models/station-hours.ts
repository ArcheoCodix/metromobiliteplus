class Time {
    stopName: string;
    scheduledArrival: number;
    scheduledDeparture:	number;
    realtimeArrival: number;
    realtimeDeparture: number;
    realtime: boolean;

    constructor(time) {
        this.stopName = time.stopName;
        this.scheduledArrival = time.scheduledArrival;
        this.scheduledDeparture = time.scheduledDeparture;
        this.realtimeArrival = time.realtimeArrival;
        this.realtimeDeparture = time.realtimeDeparture;
        this.realtime = time.realtime;
    }
}

export class StationHours {
    desc: string;
    shortDesc: string;
    times: Time[];

    constructor(hours) {
        this.desc = hours.pattern.desc;
        this.shortDesc = hours.pattern.shortDesc;
        this.times = [];

        hours.times.forEach(time => {
            this.times.push(new Time(time));
        });
    }
}
