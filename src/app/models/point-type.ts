export enum PointType {
    agenceM,
    arret,
    autostop,
    citelib,
    dat,
    depositaire,
    recharge,
    lieux,
    MVA,
    MVC,
    PAR,
    PKG,
    PMV,
    pointArret,
    pointService,
    rue
}
