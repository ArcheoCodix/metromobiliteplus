import {Location} from './location';
import {Line} from './line';

export class NearbyStation {
    private _id: string;
    private _name: string;
    private _zone: string;
    private _lines: Line[];
    private _location: Location;

    get id(): string {
        return this._id;
    }

    get name(): string {
        return this._name;
    }

    get zone(): string {
        return this._zone;
    }

    get lines(): Line[] {
        return this._lines;
    }

    get location(): Location {
        return this._location;
    }

    constructor(reqRes, lines: Line[]) {
        this._id = reqRes.id;
        this._name = reqRes.name;
        this._zone = reqRes.zone;
        this._location = new Location(reqRes.lat, reqRes.lon);
        this._lines = [];
        for (const line of reqRes.lines) {
            const obj = lines.find(el => el.id === line);
            if (obj !== undefined) {
                if (['FLEXO', 'CHRONO', 'TRAM', 'PROXIMO'].includes(obj.type.toString())) {
                    this._lines.push(obj);
                }
            }
        }
    }
}
