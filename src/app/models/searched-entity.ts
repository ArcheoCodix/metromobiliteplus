import {Location} from './location';

export class SearchedEntity {
    id: string;
    libelle: string;
    coord: Location;

    constructor(searched: any) {
        this.id = searched.properties.id;
        this.libelle = searched.properties.LIBELLE;
        this.coord = new Location(searched.geometry.coordinates[1], searched.geometry.coordinates[0]);
    }

    public setLibelle(libelle: string) {
        this.libelle = libelle;
    }
}
