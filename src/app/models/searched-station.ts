import {SearchedEntity} from './searched-entity';

export class SearchedStation extends SearchedEntity {
    idZone: string;
    commune: string;

    constructor(searched: any) {
        super(searched);
        this.idZone = searched.properties.CODE.replace('_', ':');
        this.commune = searched.properties.COMMUNE;
    }
}
