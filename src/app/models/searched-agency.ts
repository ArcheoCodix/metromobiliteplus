import {SearchedEntity} from './searched-entity';

export class SearchedAgency extends SearchedEntity {
    address: string;
    type: string;

    constructor(searched: any) {
        super(searched);
        this.address = searched.properties.RUE;
        this.type = searched.properties.TYPE;
        super.setLibelle(searched.properties.NOM);
    }
}
