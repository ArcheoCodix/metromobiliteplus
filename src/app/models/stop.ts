import {Location} from './location';

export class Stop {
    stopId: string;
    stopName: string;
    stopCity: string;
    zoneCode: string;
    trips: number[];
    stopLocation: Location;
    zoneLocation: Location;

    constructor(stop) {
        this.stopId = stop.stopId;
        this.stopName = stop.parentStation.name;
        this.stopCity = stop.parentStation.city;
        this.zoneCode = stop.parentStation.code;
        this.trips = stop.trips;
        this.stopLocation = new Location(stop.lat, stop.lon);
        this.zoneLocation = new Location(stop.parentStation.lat, stop.parentStation.lon);
    }
}
